This folder contains all protocol buffer data structures for any gRPC interactions

The compiled python will be located in the corresponding `congregate/migration/<src-or-tool>` paths

To compile all the protobufs, run `compile.sh` from this directory